extends Button

export var path: String = ""

func _on_ChangeScene_pressed():
	get_tree().change_scene(path)


func _on_ChangeScene_mouse_entered():
	$click.play()
