extends Node2D

var arena_size: float = 5000

var wave: int = 0
var enemies: int = 0

var chasing_enemy = preload("res://Nodes/Actors/ChasingEnemy.tscn")
var charging_enemy = preload("res://Nodes/Actors/ChargingEnemy.tscn")
var shooting_enemy = preload("res://Nodes/Actors/ShootingEnemy.tscn")


func _ready():
	Signals.connect("player_died",self,"restart")
	Signals.connect("enemy_died",self,"enemy_died")
	$WaveTimer.start()


func spawn_enemy():
	enemies += 1
	var new_enemy
	if wave < 2:
		new_enemy= chasing_enemy.instance()
	elif wave < 6 and wave >= 2:
		new_enemy = RNGTools.pick([chasing_enemy,charging_enemy]).instance()
	else:
		new_enemy = RNGTools.pick([chasing_enemy,charging_enemy,shooting_enemy]).instance()
	new_enemy.color = randi()%GameInfo.colors.size()
	new_enemy.has_shield = RNGTools.pick([true,false])
	new_enemy.ignore_trigger = true
	randomize()
	new_enemy.global_position = Vector2(rand_range(-arena_size,arena_size),rand_range(-arena_size,arena_size))+GameInfo.player_position
	add_child(new_enemy)


func restart():
	GameInfo.score = wave+1
	get_tree().change_scene("res://Menus/Score.tscn")


func enemy_died():
	enemies -= 1
	if enemies == 0:
		wave += 1
		$CanvasLayer/HUD/Label.text = str("Wave: ",wave+1)
		$WaveTimer.start()


func _on_WaveTimer_timeout():
	for i in range(clamp(wave+3,3,30)):
		spawn_enemy()
	

