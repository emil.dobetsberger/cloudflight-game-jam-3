extends Node

var player_position: Vector2 = Vector2.ZERO
var colors = [Color("#0092cf"), Color("#3a00a5"), Color("#a8124a"), Color("#fd5202"), Color("#fabd00"), Color("#d1eb27")]
var low_colors = [Color("#81b2c7"), Color("#7e6c9e"), Color("#a17183"), Color("#fabea2"), Color("#fce393"), Color("#dbe78e") ]
var black = Color("#171717")
var score: int = 0
