extends Node

signal ammunition_changed(current, maxx, updated, current_selected)
signal black_ammunition_changed(current, maxx)
signal color_changed(current_color)
signal shield_energy_absorbed(amount, color_index)
signal enemy_died
signal player_died
