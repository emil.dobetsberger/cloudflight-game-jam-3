extends Area2D


export var color_index : int
export var connector: NodePath


func _ready():
	$Sprite.modulate = GameInfo.colors[color_index]
	connect("area_entered", self, "press")
	
func press(area):
	if area.is_in_group("Bullet"):
		area.get_parent().destroy()
		$Sprite.frame = 1
		get_node(connector).activate_button()
