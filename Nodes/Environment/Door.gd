extends Area2D


export var color_index : int
var closed := true

# Called when the node enters the scene tree for the first time.
func _ready():
	$Sprites/Lock.modulate = GameInfo.colors[color_index]
	connect("area_entered", self, "unlock")

func unlock(area):
	if area.is_in_group("PlayerItemUse"):
		var has_key: bool = area.get_parent().use_key(color_index)
		if has_key:
			closed = false
			call_deferred("disable_collider")

func disable_collider():
		$Sprites.hide()
		$StaticBody2D/CollisionShape2D.disabled = true
