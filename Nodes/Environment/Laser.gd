extends Node2D


export var color_index : int
var active := true

# Called when the node enters the scene tree for the first time.
func _ready():
	$Sprite.modulate = GameInfo.colors[color_index]
	var anim : AnimationPlayer = $Sprite/AnimationPlayer
	anim.play("Laser")
	connect("area_entered", self, "_on_area_entered")


func _on_area_entered(area):
		
	#which object enterd the area
	if active and area.is_in_group("Bullet"):
		var color_index_bullet = area.get_parent().color_index
		
		if (color_index == color_index_bullet 
			or area.get_parent().type == Bullet.BLACK) : #(black bullets don't go through)
			area.get_parent().destroy()

func activate_button():
	call_deferred("disable")

func disable():
	active = false
	$Sprite.hide()
	$StaticBody2D/CollisionShape2D.disabled = true
	
