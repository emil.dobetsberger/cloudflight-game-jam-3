extends Area2D


export var color_index : int

func _ready():
	$Sprite.modulate = GameInfo.colors[color_index]
