extends Control

var current_ammuntion = 0
var max_ammunition = 0

func _ready():
	Signals.connect("ammunition_changed",self,"update_color_counter")
	Signals.connect("black_ammunition_changed",self,"update_black_counter")


func update_black_counter(cur, maxx):
	$Black.text = "%d/%d" % [cur, maxx]

func update_color_counter(cur, maxx, updated, current_selected):
	if updated == current_selected:
		current_ammuntion = cur
		$Color.text = "%d/%d" % [cur, maxx]
