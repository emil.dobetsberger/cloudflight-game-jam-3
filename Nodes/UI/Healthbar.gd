extends Control

onready var health_over = $HealthbarOver
onready var health_under = $HealthbarUnder
onready var update_tween = $UpdateTween

func _on_health_updated(health):
	health_over.value = health
	update_tween.interpolate_property(health_under, "value", 
									health_under.value, health, 0.5, 
									Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	
	update_tween.start()

func _on_max_health_updated(max_health):
	health_over.max_value = max_health
	health_under.max_value = max_health
