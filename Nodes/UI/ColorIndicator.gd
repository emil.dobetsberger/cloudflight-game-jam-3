extends Control

var textures = [preload("res://Resources/Sprites/UI/color wheel selection0.png"), 
				preload("res://Resources/Sprites/UI/color wheel selection1.png"),
				preload("res://Resources/Sprites/UI/color wheel selection2.png"),
				preload("res://Resources/Sprites/UI/color wheel selection3.png"),
				preload("res://Resources/Sprites/UI/color wheel selection4.png"),
				preload("res://Resources/Sprites/UI/color wheel selection5.png")]

func _ready():
	$Selection.texture = textures[2]
	Signals.connect("color_changed",self,"update_selection")

func update_selection(current_color):
	$Selection.texture = textures[current_color]

