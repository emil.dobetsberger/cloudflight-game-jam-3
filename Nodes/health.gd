extends Area2D

signal health_updated(health)
signal killed()

export (int) var max_health = 100
export (int) var remains_drop = 10

onready var health = max_health setget _set_health
onready var res_remains = preload("res://Nodes/Actors/EnemyRemains.tscn")

func damage_healing(amount):
	_set_health(health - amount)
	print(health)
	
	
#called if health drops below 0 
func kill():
	Signals.emit_signal("enemy_died")
	#making enemys "splat"
	call_deferred("spawn_remains")
	get_parent().queue_free()

func spawn_remains():
	var instance_remains = res_remains.instance()
	get_tree().root.add_child(instance_remains)
	instance_remains.position = get_parent().position
	instance_remains.set_color(get_parent().color)
	instance_remains.amount = remains_drop

#calculating the health of an entity
func _set_health(value):
	var prev_health = health
	health = clamp(value, 0, max_health)
	if health != prev_health:
		$Healthbar._on_health_updated(health)
		emit_signal("health_updated", health)
		if health == 0:
			kill()
			emit_signal("killed")

#bullet on hit
func _on_health_area_entered(area):
	#which object enterd the area
	if area.is_in_group("Bullet"):
		area.get_parent().destroy()
		var color_index_bullet = area.get_parent().color_index
		var color_index_enemy = get_parent().color
		
		
		if (get_parent().has_shield and color_index_enemy == color_index_bullet) or area.get_parent().type == Bullet.BLACK:
			return # prevent any damage
		
		var bullet_damage = area.get_parent().damage
		if (color_index_bullet + GameInfo.colors.size()/2) == color_index_enemy or (color_index_bullet - GameInfo.colors.size()/2) == color_index_enemy: # complementary
			damage_healing(bullet_damage * 3)
			print("crit")
		else:
			damage_healing(bullet_damage)
			print("normal")
