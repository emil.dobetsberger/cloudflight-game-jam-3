extends Area2D

signal health_updated(health)
signal killed()

export (int) var max_health = 100
export (int) var remains_drop = 10

onready var health = max_health setget _set_health
onready var res_remains = preload("res://Nodes/Actors/EnemyRemains.tscn")

func damage_healing(amount):
	_set_health(health - amount)
	print(health)
	
	
#called if health drops below 0 
func kill():
	if get_parent().is_in_group("Player"):
		Signals.emit_signal("player_died")
	#making enemys "splat"
	if get_parent().is_in_group("Enemy"):
		var  instance_remains = res_remains.instance()
		get_tree().root.add_child(instance_remains)
		instance_remains.position = get_parent().position
		instance_remains.set_color(get_parent().color)
		instance_remains.amount = remains_drop
		get_parent().queue_free()
		
#calculating the health of an entity
func _set_health(value):
	var prev_health = health
	health = clamp(value, 0, max_health)
	if health != prev_health:
		$Healthbar._on_health_updated(health)
		emit_signal("health_updated", health)
		if health == 0:
			kill()
			emit_signal("killed")

#bullet on hit
func _on_health_area_entered(area):
	if area.is_in_group("EnemyBullet"):
		area.get_parent().destroy()
		damage_healing(10)
		
	elif area.is_in_group("EnemyBody"): 
		damage_healing(10)
	elif area.is_in_group("ChasingEnemy"):
		damage_healing(10)
		area.get_parent()._chasingEnemyEnterd()
