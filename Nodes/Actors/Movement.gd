extends Node2D

export var speed: float = 100
export var acceleration: float = 0.2
export var deceleration: float = 0.1
export var dash_speed: float = 1500
export var dash_duration: float = 0.3


var player_movement: Vector2 = Vector2.ZERO
var player_movement_dash: Vector2 = Vector2.ZERO


signal dashed


var state: String = "moving"
var vel: Vector2 = Vector2.ZERO


func _physics_process(delta):
	get_velocity()
	apply_movement()


func get_velocity():
	if Input.is_action_pressed("down"):
		vel.y = 1
	elif Input.is_action_pressed("up"):
		vel.y = -1
	else:
		vel.y = 0
	if Input.is_action_pressed("left"):
		vel.x = -1
	elif Input.is_action_pressed("right"):
		vel.x = 1
	else:
		vel.x = 0


func apply_movement():
	player_movement = lerp(player_movement,vel.normalized()*speed,acceleration) if vel != Vector2.ZERO else lerp(player_movement,vel.normalized()*speed,deceleration)


func dash():
	$whoosh.play()
	player_movement_dash = vel.normalized()*dash_speed
	$Dash.interpolate_property(self,"player_movement_dash",player_movement_dash,vel*speed,dash_duration,Tween.TRANS_CUBIC,Tween.EASE_OUT)
	$Dash.start()


func _on_Dash_tween_all_completed():
	emit_signal("dashed")
