extends Enemy

enum {MOVING, CHARGING}

var state = MOVING

const CHARGE_SPEED = 2500
const CHARGE_INI_DISTANCE = 1100
const SPEED = 300

var vel: Vector2 = Vector2.ZERO
var distance: float = 10000
var charge_speed: float = 0
var charge_dir: Vector2 = Vector2.ZERO


func _ready():
	$Body/Node2D/ColorIndicator.set_color(color)


func _physics_process(delta):
	if active or ignore_trigger:
		vel = GameInfo.player_position - global_position
		vel = vel.normalized()
		distance = (GameInfo.player_position - global_position).length()
		match state:
			MOVING:
				$Body.look_at(GameInfo.player_position)
				move_and_slide(vel*SPEED)
				if distance <= CHARGE_INI_DISTANCE:
					state = CHARGING
					charge_speed = 0
					$ChargeDelay.start()
			CHARGING:
				$Body.rotate(20*delta)
				move_and_slide(charge_dir*charge_speed)


func _on_ChargeDelay_timeout():
	charge_speed = CHARGE_SPEED
	charge_dir = vel
	$ChargeDuration.start()


func _on_ChargeDuration_timeout():
	state = MOVING
