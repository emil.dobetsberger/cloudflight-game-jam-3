extends KinematicBody2D

onready var shield_res = preload("res://Nodes/Actors/Shield.tscn")

class_name Enemy

export (int) var color
export (float) var shield_size = 1.0
export (bool) var has_shield
export (bool) var ignore_trigger = false
var active

func _ready():
	if has_shield:
		var shield = shield_res.instance()
		add_child(shield)
		shield.scale = Vector2(shield_size, shield_size)
		shield.set_color(color)

func trigger(area):
	if area.is_in_group("Player"):
		active = true

func untrigger(area):
	if area.is_in_group("Player"):
		active = false
