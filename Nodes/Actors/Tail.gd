extends Node2D

func _physics_process(delta):
	$Body.look_at(get_global_mouse_position())
	$TailParts/Part1.look_at($Body.global_position)
	$TailParts/Part1.global_position = lerp($TailParts/Part1.global_position,$Body/Pos1.global_position,0.4)
	$TailParts/Part2.look_at($Body/Pos1.global_position)
	$TailParts/Part2.global_position = lerp($TailParts/Part2.global_position,$Body/Pos1/Pos2.global_position,0.3)
