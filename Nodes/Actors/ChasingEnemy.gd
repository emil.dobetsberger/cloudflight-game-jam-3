extends Enemy

var vel: Vector2 = Vector2.ZERO
var speed: float = 500

var bias_range: float = 200
var bias: Vector2 = Vector2.ZERO
var target_pos: Vector2 = Vector2.ZERO

var chasingTimer = Timer.new()

func _ready():
	$Body/Node2D/ColorIndicator.set_color(color)
	randomize()
	bias = Vector2(rand_range(-bias_range,bias_range),rand_range(-bias_range,bias_range))
	
	#timer stuff
	_disableHurtbox()
	chasingTimer.autostart = false
	chasingTimer.set_wait_time(1.5)
	chasingTimer.set_one_shot(true)
	add_child(chasingTimer)
	chasingTimer.connect("timeout", self, "_disableHurtbox")

func _physics_process(delta):
	if active or ignore_trigger:
		$Body.look_at(GameInfo.player_position)
		vel = GameInfo.player_position - global_position
		vel = vel.normalized()
		move_and_slide(vel*speed+bias)

func _chasingEnemyEnterd():
	chasingTimer.start()
	call_deferred("_showDebug")
	
func _disableHurtbox():
	$Hurtbox/Hurtbox.disabled = false
	
func _showDebug():
	$Hurtbox/Hurtbox.disabled = true 
