extends Area2D

export var color_index : int
export var amount : int

func _ready():
	set_color(color_index)

func set_color(index):
	color_index = index
	$Sprite.modulate = GameInfo.colors[index]
