extends Area2D

onready var particle_effect: CPUParticles2D = $CPUParticles2D
var color_index
const MAX_ENERGY = 100
var cur_energy

func _ready():
	cur_energy = MAX_ENERGY

func set_color(color):
	color_index = color
	$Sprite.modulate = GameInfo.colors[color]
	set_particle_color()
	connect("area_entered", self, "_on_area_entered")
	
func _on_area_entered(area: Area2D):
	if area.is_in_group("Bullet"):
		if area.get_parent().type == Bullet.BLACK:
			deplete_energy(area.get_parent().damage)
			area.get_parent().destroy()
		else:
			var color_index_bullet = area.get_parent().color_index
			if color_index == color_index_bullet:
				particle_effect.restart()
				area.get_parent().destroy()

func deplete_energy(amount):
	amount = min(cur_energy, amount) # don't go below 0
	cur_energy -= amount
	Signals.emit_signal("shield_energy_absorbed", amount, color_index)
	if cur_energy <= 0:
		get_parent().has_shield = false
		call_deferred("disable_shield")
	else:
		$Sprite.modulate = GameInfo.low_colors[color_index].linear_interpolate(GameInfo.colors[color_index], (cur_energy+0.0)/MAX_ENERGY)
		
func disable_shield():
	$Sprite.modulate = Color(GameInfo.black.to_rgba32())
	$Sprite.modulate.a = 0.3
	$CollisionShape2D.disabled = true

func set_particle_color():
	particle_effect.color_ramp = particle_effect.color_ramp.duplicate()
	particle_effect.color_ramp.colors[0] = Color(GameInfo.colors[color_index].to_rgba32())
	var transparent := Color(GameInfo.colors[color_index].to_rgba32())
	transparent.a = 0
	particle_effect.color_ramp.colors[1] = transparent
