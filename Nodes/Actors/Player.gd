extends KinematicBody2D

enum {IDLE, RELOADING, DASHING}

var state = IDLE
var velocity: Vector2 = Vector2.ZERO

var current_color = 0

var keys = []

func _ready():
	$Movement.connect("dashed",self,"go_idle")
	$Shooting.connect("reloaded",self,"go_idle")
	change_color(0)

func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed() and state != RELOADING:
			if event.button_index == BUTTON_WHEEL_UP:
				change_color(-1)
			elif event.button_index == BUTTON_WHEEL_DOWN:
				change_color(1)

func _physics_process(delta):
	
	GameInfo.player_position = global_position
	$Body.look_at(get_global_mouse_position())
	
	if state != DASHING:
		velocity = $Movement.player_movement
		$Body/Sprite.modulate = Color(1, 1, 1, 1)
	match state:
		IDLE:
			if Input.is_action_just_pressed("dash") and $Movement.vel != Vector2.ZERO:
				state = DASHING
				$Movement.dash()
			elif Input.is_action_just_pressed("reload"):
				state = RELOADING
				$Shooting.start_reload()
			elif Input.is_action_pressed("shoot"):
				$Shooting.start_shoot(false)
			elif Input.is_action_pressed("shoot_black"):
				$Shooting.start_shoot(true)
		RELOADING:
			$Body/Sprite.modulate = Color(1, 1, 0, 1)
			pass
		DASHING:
#			$Body/Sprite.modulate = Color(1, 1, 1, 0.5)
			velocity = $Movement.player_movement_dash
	move_and_slide(velocity)


func go_idle():
	state = IDLE
	
func change_color(delta):
	current_color = (current_color + delta) % GameInfo.colors.size()
	if current_color == -1: 
		current_color = GameInfo.colors.size() - 1
	$Body/ColorIndicator.modulate = GameInfo.colors[current_color]
	$Shooting.set_color(current_color)
	Signals.emit_signal("color_changed", current_color)

func add_item(area):
	if area.is_in_group("EnemyRemains"):
		$Shooting.pickup_remains(area)
		$Shooting/charge.play()
	if area.is_in_group("Key"):
		add_key(area.color_index)
		area.queue_free()

func add_key(color_index):
	keys.append(color_index)
	
func use_key(color_index) -> bool:
	var index = keys.find(color_index)
	if index != -1:
		keys.remove(index)
		return true
	return false
