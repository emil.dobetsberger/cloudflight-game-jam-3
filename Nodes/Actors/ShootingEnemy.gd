extends Enemy

const DESIRED_DISTANCE = 800
const SPEED = 300

var bias: Vector2 = Vector2.ZERO
var target_pos: Vector2 = Vector2.ZERO
var angle_to_player: float = 0

var bullet = preload("res://Nodes/Shooting/EnemyBullet.tscn")

func _ready():
	$Body/Node2D/ColorIndicator.set_color(color)
	randomize()
	$ShootTimer.wait_time = rand_range(2,8)
	$ShootTimer.start()


func _physics_process(delta):
	if active or ignore_trigger:
		$Body.look_at(GameInfo.player_position)
		target_pos = GameInfo.player_position + ( global_position - GameInfo.player_position).normalized()*DESIRED_DISTANCE
		target_pos += Vector2.UP.rotated(deg2rad(angle_to_player))*100
		$Node/point/Sprite.global_position = target_pos
		global_position = lerp(global_position,target_pos,0.05)


func shoot():
	for i in range(3):
		var new_bullet = bullet.instance()
		get_tree().root.add_child(new_bullet)
		new_bullet.global_position = global_position
		new_bullet.direction = (GameInfo.player_position - global_position).normalized()
		if i == 0:
			new_bullet.direction = (GameInfo.player_position - global_position).normalized()
		elif i == 1:
			new_bullet.direction = ((GameInfo.player_position - global_position).normalized()).rotated(25)
		elif i == 2:
			new_bullet.direction = ((GameInfo.player_position - global_position).normalized()).rotated(-25)
		new_bullet.speed = 600


func _on_ChangeAngleToPlayer_timeout():
	randomize()
	angle_to_player = rand_range(0,360)


func _on_ShootTimer_timeout():
	if active or ignore_trigger:
		randomize()
		$ShootTimer.wait_time = rand_range(2,8)
		shoot()
