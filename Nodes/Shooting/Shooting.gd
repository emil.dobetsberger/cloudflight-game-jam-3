extends Node2D

onready var instanced_bullet = preload("res://Nodes/Shooting/Bullet.tscn")

onready var shot_timer : Timer = $ShotTimer
onready var reload_timer : Timer = $ReloadTimer
onready var shot_buffer_timer : Timer = $InputBufferTimer

export var ammo_dict = {"max_0": 500, "cur_0": 0,
				"max_1": 500, "cur_1": 0, 
				"max_2": 500, "cur_2": 0,
				"max_3": 500, "cur_3": 0,
				"max_4": 500, "cur_4": 0,
				"max_5": 500, "cur_5": 0,
				"max_6": 500, "cur_6": 0,
				"max_black": 100, "cur_black":100
				}

export var bullet_speed : int = 800
export var bullet_spawn_offset : int = 30
export var fire_rate : float = 12
export var reload_time : float = 1.5
export var shot_buffer_time : float = 0.1
export var bullet_spread : float = 0.1
export var bullet_damage : int = 2
export var shield_damage : int = 5
export var shield_color_steal_per_hit : int = 5
var aim_point : Vector2 
var color_index

var left_right_gun: bool = false

signal reloaded

func _ready():
	set_black_ammo(ammo_dict["max_black"])
	Signals.connect("shield_energy_absorbed", self, "absorb_color")

func _process(delta):
	aim_point = get_global_mouse_position()
	#if shot_buffer_timer.time_left > 0 && shot_timer.time_left == 0:
	#	shoot()
	
	
func start_shoot(black: bool):
	if shot_timer.time_left == 0:
		if black:
			shoot_black()
		else:
			shoot_color()
	#shot_buffer_timer.start(shot_buffer_time)

func shoot_black():
	var cur_ammo = ammo_dict["cur_black"]
	if cur_ammo >= 1:
		shot_timer.start(1.0/fire_rate)
		set_black_ammo(cur_ammo-1)
		var bullet : Node2D = instanced_bullet.instance()
		bullet.get_node("Sprite").modulate = GameInfo.black
		spawn_bullet(bullet)
		bullet.type = Bullet.BLACK
		bullet.damage = shield_damage
	else:
		print("no ammo")

func shoot_color():
	#shot_buffer_timer.stop()
	var cur_ammo = get_cur_ammo()
	if cur_ammo >= 1:
		shot_timer.start(1.0/fire_rate)
		set_color_ammo(cur_ammo-1)
		var bullet : Node2D = instanced_bullet.instance()
		bullet.get_node("Sprite").modulate = GameInfo.colors[color_index]
		spawn_bullet(bullet)
		bullet.damage = bullet_damage
	else:
		print("no ammo")
	

func spawn_bullet(bullet : Node2D):
	$shoot.pitch_scale = rand_range(0.95,1.05)
	$shoot.play()
	get_tree().root.add_child(bullet)
	$Body.look_at(get_global_mouse_position())
	var aim_direction = aim_point - get_parent().position
	var spawn_pos

	if left_right_gun:
		spawn_pos = $Body/Gun1.global_position + aim_direction.normalized() * bullet_spawn_offset
		get_node("Fire").play("Fire")
	else:
		spawn_pos = $Body/Gun2.global_position + aim_direction.normalized() * bullet_spawn_offset
		get_node("Fire").play("Fire2")
	bullet.position = spawn_pos
	left_right_gun = !left_right_gun
	bullet.speed = bullet_speed
	var direction = aim_point - get_parent().global_position
	direction = direction.normalized()
	var spread = Vector2(direction.y, -direction.x)
	bullet.direction = direction + spread * rand_range(-1,1) * bullet_spread
	bullet.color_index = color_index
		
func start_reload():
	$charge.play()
	reload_timer.start(reload_time)


func finish_reload():
	set_black_ammo(ammo_dict["max_black"])
	emit_signal("reloaded")
	
func get_cur_ammo():
	return ammo_dict["cur_%d" % color_index]
	

func set_black_ammo(val):
	ammo_dict["cur_black"] = val
	Signals.emit_signal("black_ammunition_changed", val, ammo_dict["max_black"])
	

func set_color_ammo(val):
	ammo_dict["cur_%d" % color_index] = val
	Signals.emit_signal("ammunition_changed", val, ammo_dict["max_%d" % color_index], color_index, color_index)

func set_color(color_index):
	self.color_index = color_index
	Signals.emit_signal("ammunition_changed", ammo_dict["cur_%d" % color_index], ammo_dict["max_%d" % color_index], color_index, color_index)
	


func pickup_remains(remains):
	var remains_color = remains.color_index
	var remains_ammo_amount = remains.amount
	update_color(remains_ammo_amount, remains_color)
	remains.queue_free()

func update_color(amount, index):
	var max_ammo = ammo_dict["max_%d" % index]
	var cur_ammo = ammo_dict["cur_%d" % index]
	if cur_ammo != max_ammo:
		ammo_dict["cur_%d" % index] = min(cur_ammo + amount, max_ammo)
		Signals.emit_signal("ammunition_changed", ammo_dict["cur_%d" % index], max_ammo, index, color_index)
		
func absorb_color(depleted_amount, index):
	var add_amount = (0.0 + depleted_amount * shield_color_steal_per_hit) / shield_damage
	update_color(floor(add_amount), index)
