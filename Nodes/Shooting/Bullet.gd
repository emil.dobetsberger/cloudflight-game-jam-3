extends Node2D

onready var splash_particles = preload("res://Nodes/Shooting/BulletSplash.tscn")

class_name Bullet

enum {COLOR, BLACK}

var speed : int
var direction : Vector2
var color_index : int
var damage : int
var type = COLOR

func _physics_process(delta):
	position = position + speed*direction*delta

func destroy():
	call_deferred("spawn_particles", position)
	
	queue_free()

func spawn_particles(pos):
	var color = GameInfo.colors[color_index] if type == COLOR else GameInfo.black
	var splash_instance : CPUParticles2D = splash_particles.instance()
	splash_instance.color_ramp = splash_instance.color_ramp.duplicate()
	splash_instance.color_ramp.colors[0] = Color(color.to_rgba32())
	var transparent := Color(color.to_rgba32())
	transparent.a = 0
	splash_instance.color_ramp.colors[1] = transparent
	get_parent().add_child(splash_instance)
	splash_instance.position = pos
	splash_instance.restart()


func _on_Area2D_body_entered(body):
	destroy()
	pass
